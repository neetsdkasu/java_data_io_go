package java_data_io_go

import (
	"bufio"
	"encoding/binary"
	"errors"
	"io"
	"math"
	"unicode/utf16"
)

type JavaDataInput struct {
	reader *bufio.Reader
	buf    []byte
}

type JavaDataOutput struct {
	writer io.Writer
	buf    []byte
}

var UTFDataFormatError = errors.New("UTFDataFormatError")

type JavaChar = uint16
type JavaString = []uint16

func ToJavaString(s string) JavaString {
	return utf16.Encode([]rune(s))
}

func FromJavaString(s JavaString) string {
	return string(utf16.Decode(s))
}

func NewJavaDataInput(reader io.Reader) *JavaDataInput {
	buf := [8]byte{}
	if r, ok := reader.(*bufio.Reader); ok {
		return &JavaDataInput{r, buf[:]}
	} else {
		return &JavaDataInput{bufio.NewReader(reader), buf[:]}
	}
}

func NewJavaDataOutput(writer io.Writer) *JavaDataOutput {
	buf := [8]byte{}
	return &JavaDataOutput{writer, buf[:]}
}

func (self *JavaDataInput) Read(buf []byte) (n int, err error) {
	return self.reader.Read(buf)
}

func (self *JavaDataInput) ReadFully(buf []byte) (err error) {
	_, err = io.ReadFull(self.reader, buf)
	return
}

func (self *JavaDataInput) ReadDouble() (v float64, err error) {
	// err = binary.Read(self.reader, binary.BigEndian, &v)
	_, err = io.ReadFull(self.reader, self.buf)
	if err == nil {
		v = math.Float64frombits(binary.BigEndian.Uint64(self.buf))
	}
	return
}

func (self *JavaDataInput) ReadFloat() (v float32, err error) {
	// err = binary.Read(self.reader, binary.BigEndian, &v)
	_, err = io.ReadFull(self.reader, self.buf[:4])
	if err == nil {
		v = math.Float32frombits(binary.BigEndian.Uint32(self.buf))
	}
	return
}

func (self *JavaDataInput) ReadLong() (v int64, err error) {
	// err = binary.Read(self.reader, binary.BigEndian, &v)
	_, err = io.ReadFull(self.reader, self.buf)
	if err == nil {
		v = int64(binary.BigEndian.Uint64(self.buf))
	}
	return
}

func (self *JavaDataInput) ReadInt() (v int32, err error) {
	// err = binary.Read(self.reader, binary.BigEndian, &v)
	_, err = io.ReadFull(self.reader, self.buf[:4])
	if err == nil {
		v = int32(binary.BigEndian.Uint32(self.buf))
	}
	return
}

func (self *JavaDataInput) ReadShort() (v int16, err error) {
	// err = binary.Read(self.reader, binary.BigEndian, &v)
	_, err = io.ReadFull(self.reader, self.buf[:2])
	if err == nil {
		v = int16(binary.BigEndian.Uint16(self.buf))
	}
	return
}

func (self *JavaDataInput) ReadUnsignedShort() (v uint16, err error) {
	// err = binary.Read(self.reader, binary.BigEndian, &v)
	_, err = io.ReadFull(self.reader, self.buf[:2])
	if err == nil {
		v = binary.BigEndian.Uint16(self.buf)
	}
	return
}

func (self *JavaDataInput) ReadByte() (v int8, err error) {
	// err = binary.Read(self.reader, binary.BigEndian, &v)
	var b byte
	b, err = self.reader.ReadByte()
	if err == nil {
		v = int8(b)
	}
	return
}

func (self *JavaDataInput) ReadUnsignedByte() (v uint8, err error) {
	// err = binary.Read(self.reader, binary.BigEndian, &v)
	var b byte
	b, err = self.reader.ReadByte()
	if err == nil {
		v = uint8(b)
	}
	return
}

func (self *JavaDataInput) ReadBoolean() (v bool, err error) {
	// err = binary.Read(self.reader, binary.BigEndian, &v)
	var b byte
	b, err = self.reader.ReadByte()
	if err == nil {
		v = b != 0
	}
	return
}

func (self *JavaDataInput) ReadChar() (v JavaChar, err error) {
	// err = binary.Read(self.reader, binary.BigEndian, &v)
	_, err = io.ReadFull(self.reader, self.buf[:2])
	if err == nil {
		v = binary.BigEndian.Uint16(self.buf)
	}
	return
}

func (self *JavaDataInput) ReadUTF() (JavaString, error) {
	size, err := self.ReadUnsignedShort()
	if err != nil {
		return nil, err
	}
	var count = int32(size)
	var a, b, c byte
	ret := JavaString{}
	for count > 0 {
		if a, err = self.reader.ReadByte(); err != nil {
			return nil, err
		}
		if (a & 0xF0) == 0xF0 {
			return nil, UTFDataFormatError
		}
		if (a & 0xC0) == 0x80 {
			return nil, UTFDataFormatError
		}
		if a < 0x80 {
			ret = append(ret, uint16(a))
			count--
		} else if a < (0x80 | 0x40 | 0x20) {
			if b, err = self.reader.ReadByte(); err != nil {
				return nil, err
			}
			if (b & 0xC0) != 0x80 {
				return nil, UTFDataFormatError
			}
			ret = append(ret, (uint16(0x1F&a)<<6)|uint16(0x3F&b))
			count -= 2
		} else {
			if b, err = self.reader.ReadByte(); err != nil {
				return nil, err
			}
			if (b & 0xC0) != 0x80 {
				return nil, UTFDataFormatError
			}
			if c, err = self.reader.ReadByte(); err != nil {
				return nil, err
			}
			if (c & 0xC0) != 0x80 {
				return nil, UTFDataFormatError
			}
			ret = append(ret, (uint16(0x0F&a)<<12)|(uint16(0x3F&b)<<6)|uint16(0x3F&c))
			count -= 3
		}
	}
	if count < 0 {
		return nil, UTFDataFormatError
	}
	return ret, nil
}

func (self *JavaDataInput) ReadLine() (JavaString, error) {
	var b byte
	var err error
	ret := JavaString{}
	for {
		if b, err = self.reader.ReadByte(); err != nil {
			if err == io.EOF {
				if len(ret) == 0 {
					return nil, nil
				}
				break
			}
			return nil, err
		}
		if b == '\n' {
			break
		}
		if b == '\r' {
			if b, err = self.reader.ReadByte(); err != nil {
				if err != io.EOF {
					return nil, err
				}
			} else if b != '\n' {
				if err = self.reader.UnreadByte(); err != nil {
					return nil, err
				}
			}
			break
		}
		ret = append(ret, uint16(b))
	}
	return ret, nil
}

func (self *JavaDataInput) SkipBytes(n int32) (int32, error) {
	count, err := self.reader.Discard(int(n))
	if err == io.EOF {
		err = nil
	}
	return int32(count), err
}

func (self *JavaDataOutput) Write(buf []byte) (int, error) {
	return self.writer.Write(buf)
}

func (self *JavaDataOutput) WriteDouble(v float64) (err error) {
	// err = binary.Write(self.writer, binary.BigEndian, v)
	binary.BigEndian.PutUint64(self.buf, math.Float64bits(v))
	_, err = self.writer.Write(self.buf)
	return
}

func (self *JavaDataOutput) WriteFloat(v float32) (err error) {
	// err = binary.Write(self.writer, binary.BigEndian, v)
	binary.BigEndian.PutUint32(self.buf, math.Float32bits(v))
	_, err = self.writer.Write(self.buf[:4])
	return
}

func (self *JavaDataOutput) WriteLong(v int64) (err error) {
	// err = binary.Write(self.writer, binary.BigEndian, v)
	binary.BigEndian.PutUint64(self.buf, uint64(v))
	_, err = self.writer.Write(self.buf)
	return
}

func (self *JavaDataOutput) WriteInt(v int32) (err error) {
	// err = binary.Write(self.writer, binary.BigEndian, v)
	binary.BigEndian.PutUint32(self.buf, uint32(v))
	_, err = self.writer.Write(self.buf[:4])
	return
}

func (self *JavaDataOutput) WriteShort(v int32) (err error) {
	// err = binary.Write(self.writer, binary.BigEndian, uint16(v&0xFFFF))
	binary.BigEndian.PutUint16(self.buf, uint16(v&0xFFFF))
	_, err = self.writer.Write(self.buf[:2])
	return
}

func (self *JavaDataOutput) WriteByte(v int32) (err error) {
	// err = binary.Write(self.writer, binary.BigEndian, byte(v&0xFF))
	self.buf[0] = byte(v & 0xFF)
	_, err = self.writer.Write(self.buf[:1])
	return
}

func (self *JavaDataOutput) WriteBoolean(v bool) (err error) {
	// err = binary.Write(self.writer, binary.BigEndian, v)
	if v {
		self.buf[0] = 1
	} else {
		self.buf[0] = 0
	}
	_, err = self.writer.Write(self.buf[:1])
	return
}

func (self *JavaDataOutput) WriteChar(v int32) (err error) {
	// err = binary.Write(self.writer, binary.BigEndian, JavaChar(v&0xFFFF))
	binary.BigEndian.PutUint16(self.buf, uint16(v&0xFFFF))
	_, err = self.writer.Write(self.buf[:2])
	return
}

func (self *JavaDataOutput) WriteBytes(s JavaString) (err error) {
	buf := self.buf[:1]
	for _, c := range s {
		buf[0] = byte(c & 0xFF)
		_, err = self.writer.Write(buf)
		if err != nil {
			return
		}
	}
	return
}

func (self *JavaDataOutput) WriteChars(s JavaString) (err error) {
	buf := self.buf[:2]
	for _, c := range s {
		binary.BigEndian.PutUint16(buf, c)
		_, err = self.writer.Write(buf)
		if err != nil {
			return
		}
	}
	return
}

func (self *JavaDataOutput) WriteUTF(s JavaString) (err error) {
	var size = 0
	for _, c := range s {
		if 0x0000 < c && c < 0x0080 {
			size++
		} else if c < 0x0800 {
			size += 2
		} else {
			size += 3
		}
	}
	if size > 0xFFFF {
		return UTFDataFormatError
	}
	err = self.WriteShort(int32(size))
	if err != nil {
		return
	}
	buf := self.buf
	var s1 = buf[:1]
	var s2 = buf[:2]
	var s3 = buf[:3]
	for _, c := range s {
		if 0x0000 < c && c < 0x0080 {
			buf[0] = byte(c & 0xFF)
			_, err = self.writer.Write(s1)
			if err != nil {
				return
			}
		} else if c < 0x0800 {
			buf[0] = 0xC0 | byte(0x1F&(c>>6))
			buf[1] = 0x80 | byte(0x3F&c)
			_, err = self.writer.Write(s2)
			if err != nil {
				return
			}
		} else {
			buf[0] = 0xE0 | byte(0x0F&(c>>12))
			buf[1] = 0x80 | byte(0x3F&(c>>6))
			buf[2] = 0x80 | byte(0x3F&c)
			_, err = self.writer.Write(s3)
			if err != nil {
				return
			}
		}
	}
	return
}
