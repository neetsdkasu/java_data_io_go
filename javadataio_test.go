package java_data_io_go

import (
	"bytes"
	"testing"
)

func TestDataIO(t *testing.T) {
	src := [...]int8{
		0, 11, -29, -127, -126, -29, -127, -124, -29, -127, -122, 68, 120, -56, 66, 35, -19,
		-94, 68, -7, -66, -80, -121, -67, -1, 0, 0, 18, 8, 0, 1, -122, -89, 0, 1, -122, -89,
		-79, -41, -3, -23, -128, -6, 1, 0, 97, 98, 99, 100, 101, 49, 50, 51, 52, 53, 83, 65,
		77, 80, 76, 69, 10, 115, 97, 109, 112, 108, 101, 10, 65, 66, 67, 68, 69,
	}

	buf := make([]byte, len(src))
	for i, s := range src {
		buf[i] = byte(s)
	}

	jdi := NewJavaDataInput(bytes.NewReader(buf))

	if v, err := jdi.ReadUTF(); err != nil {
		t.Fatal(err)
	} else if s := FromJavaString(v); s != "あいうDx" {
		t.Fatal("invalid ReadUTF:", v, s)
	}

	if v, err := jdi.ReadDouble(); err != nil {
		t.Fatal(err)
	} else if v != -1.23456789e40 {
		t.Fatal("invalid ReadDouble", v)
	}

	if v, err := jdi.ReadFloat(); err != nil {
		t.Fatal(err)
	} else if v != -9.87654321e-10 {
		t.Fatal("invalid ReadFloat", v)
	}

	const LV = 100007 + (1 << 35) + (1 << 41) + (1 << 44)
	if v, err := jdi.ReadLong(); err != nil {
		t.Fatal(err)
	} else if v != LV {
		t.Fatal("invalid ReadLong", v)
	}

	if v, err := jdi.ReadInt(); err != nil {
		t.Fatal(err)
	} else if v != 100007 {
		t.Fatal("invalid ReadInt", v)
	}

	if v, err := jdi.ReadShort(); err != nil {
		t.Fatal(err)
	} else if v != -20009 {
		t.Fatal("invalid ReadShort", v)
	}

	if v, err := jdi.ReadUnsignedShort(); err != nil {
		t.Fatal(err)
	} else if v != 65001 {
		t.Fatal("invalid ReadUnsignedShort", v)
	}

	if v, err := jdi.ReadByte(); err != nil {
		t.Fatal(err)
	} else if v != -128 {
		t.Fatal("invalid ReadByte", v)
	}

	if v, err := jdi.ReadUnsignedByte(); err != nil {
		t.Fatal(err)
	} else if v != 250 {
		t.Fatal("invalid ReadUnsignedByte", v)
	}

	if v, err := jdi.ReadBoolean(); err != nil {
		t.Fatal(err)
	} else if v != true {
		t.Fatal("invalid ReadBoolean (TRUE)", v)
	}

	if v, err := jdi.ReadBoolean(); err != nil {
		t.Fatal(err)
	} else if v != false {
		t.Fatal("invalid ReadBoolean (FALSE)", v)
	}

	var r [10]byte
	if err := jdi.ReadFully(r[:]); err != nil {
		t.Fatal(err)
	} else if !bytes.Equal(r[:], []byte("abcde12345")) {
		t.Fatal("invalid ReadFully", r)
	}

	if v, err := jdi.ReadLine(); err != nil {
		t.Fatal(err)
	} else if s := FromJavaString(v); s != "SAMPLE" {
		t.Fatal("invalid ReadLine", v, s)
	}

	if v, err := jdi.ReadLine(); err != nil {
		t.Fatal(err)
	} else if s := FromJavaString(v); s != "sample" {
		t.Fatal("invalid ReadLine2", v, s)
	}

	if v, err := jdi.ReadLine(); err != nil {
		t.Fatal(err)
	} else if s := FromJavaString(v); s != "ABCDE" {
		t.Fatal("invalid ReadLine3", v, s)
	}

	var dst bytes.Buffer

	jdo := NewJavaDataOutput(&dst)

	if err := jdo.WriteUTF(ToJavaString("あいうDx")); err != nil {
		t.Fatal(err)
	}

	if err := jdo.WriteDouble(-1.23456789e40); err != nil {
		t.Fatal(err)
	}

	if err := jdo.WriteFloat(-9.87654321e-10); err != nil {
		t.Fatal(err)
	}

	if err := jdo.WriteLong(LV); err != nil {
		t.Fatal(err)
	}

	if err := jdo.WriteInt(100007); err != nil {
		t.Fatal(err)
	}

	if err := jdo.WriteShort(-20009); err != nil {
		t.Fatal(err)
	}

	if err := jdo.WriteShort(65001); err != nil {
		t.Fatal(err)
	}

	if err := jdo.WriteByte(-128); err != nil {
		t.Fatal(err)
	}

	if err := jdo.WriteByte(250); err != nil {
		t.Fatal(err)
	}

	if err := jdo.WriteBoolean(true); err != nil {
		t.Fatal(err)
	}

	if err := jdo.WriteBoolean(false); err != nil {
		t.Fatal(err)
	}

	if err := jdo.WriteBytes(ToJavaString("abcde12345")); err != nil {
		t.Fatal(err)
	}

	if err := jdo.WriteBytes(ToJavaString("SAMPLE\nsample\nABCDE")); err != nil {
		t.Fatal(err)
	}

	if !bytes.Equal(buf, dst.Bytes()) {
		t.Fatal("unmatch! buf != dst")
	}

	src2 := [...]int8{48, -94, 48, -85, 48, -75, 0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16, 17, 18, 19, 20, 21, 22, 23, 24, 25, 26, 27, 28, 29, 30, 48, -65, 48, -54, 48, -49, 48, -34, 48, -28, 48, -23, 48, -17}

	buf2 := make([]byte, len(src2))
	for i, s := range src2 {
		buf2[i] = byte(s)
	}

	jdi2 := NewJavaDataInput(bytes.NewReader(buf2))

	for _, ch := range ToJavaString("アカサ") {
		if c, err := jdi2.ReadChar(); err != nil {
			t.Fatal(err)
		} else if c != ch {
			t.Fatal("invalid ReadChar", ch, c)
		}
	}

	if sb, err := jdi2.SkipBytes(31); err != nil {
		t.Fatal(err)
	} else if sb != 31 {
		t.Fatal("invalid SkipBytes", sb)
	}

	for _, ch := range ToJavaString("タナハマヤラワ") {
		if c, err := jdi2.ReadChar(); err != nil {
			t.Fatal(err)
		} else if c != ch {
			t.Fatal("invalid ReadChar", ch, c)
		}
	}

	var dst2 bytes.Buffer

	jdo2 := NewJavaDataOutput(&dst2)
	for _, ch := range ToJavaString("アカサ") {
		if err := jdo2.WriteChar(int32(ch)); err != nil {
			t.Fatal(err)
		}
	}
	vs := make([]byte, 31)
	for i := range vs {
		vs[i] = byte(i)
	}
	if n, err := jdo2.Write(vs); err != nil {
		t.Fatal(err)
	} else if n != 31 {
		t.Fatal("invalid Write", n)
	}
	if err := jdo2.WriteChars(ToJavaString("タナハマヤラワ")); err != nil {
		t.Fatal(err)
	}

	if !bytes.Equal(buf2, dst2.Bytes()) {
		t.Fatal("unmatch! buf2 != dst2")
	}
}
